package goutils

import (
	"net"
	"strconv"
)


func Telnet(host string, port int) bool {
	conn, err := net.Dial("tcp", host + ":" + strconv.Itoa(port))
	if err == nil {
		defer func() {
			_ = conn.Close()
		}()
		return true
	} else {
		return false
	}
}