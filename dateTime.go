package goutils

import (
	"strings"
	"time"
)

const GoBirthTime = "2006-01-02 15:04:05"

func UnixToDate(unixtime int64) string {
	t := time.Unix(unixtime, 0)
	return t.Format("2006-01-02")
}

func UnixToDateTime(unixtime int64) string {
	t := time.Unix(unixtime, 0)
	return t.Format("2006-01-02 15:04:05")
}

func UnixToDateFormat(unixtime int64, format string) string {
	t := time.Unix(unixtime, 0)
	return t.Format(format)
}

func DateTimeToUnix(datetime string) (int64, error) {
	var format string
	switch len(datetime) {
		case 10:
			format = "2006-01-02"

		default:
			format = "2006-01-02 15:04:05"
	}
	return DateFormatToUnix(datetime, format)
}

func DateFormatToUnix(datetime, format string) (int64, error) {

	if strings.HasPrefix(datetime, "0") {
		return 0, nil
	}

	loc, _ := time.LoadLocation("Europe/Kiev")
	date, err := time.ParseInLocation(format, datetime, loc)

	if err != nil {
		return 0, err
	}
	return date.Unix(), nil
}