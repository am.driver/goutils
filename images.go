package goutils

import (
	"bufio"
	"bytes"
	"fmt"
	"github.com/nfnt/resize"
	"image"
	"image/jpeg"
	"image/png"
	"io/ioutil"
	"net/http"
)

type ImageObject struct {
	Height int
	Width int
	MimeType string
	Content []byte
	Image image.Image
}

func NewImageFile(path string) (ImageObject, error) {
	fileContent, err := ioutil.ReadFile(path)
	if err != nil {
		return ImageObject{}, err
	} else {
		return NewImage(fileContent)
	}
}

func NewImage(content []byte) (ImageObject, error) {

	img, _, err := image.Decode(bytes.NewReader(content))
	if err != nil {
		return ImageObject{}, err
	}

	imgCnf, _, errCnf := image.DecodeConfig(bytes.NewReader(content))
	if errCnf != nil {
		return ImageObject{}, errCnf
	}

	return ImageObject {
		Height:	  imgCnf.Height,
		Width:    imgCnf.Width,
		MimeType: GetContentMimeType(content),
		Content:  content,
		Image:    img,
	}, nil
}

func GetContentMimeType(content []byte) string {
	return http.DetectContentType(content)
}

func (img *ImageObject) String() string {
	return fmt.Sprintf("Image: %s %d*%d %db", img.MimeType, img.Height, img.Width, len(img.Content))
}

func (img *ImageObject) Resize(height, width int) error {
	return img.ResizeWithFormat(height, width, img.MimeType)
}

func (img *ImageObject) ResizeWithFormat(height, width int, format string) error {
	newImage := resize.Resize(uint(width), uint(height), img.Image, resize.Lanczos3)
	newContent, err := encodeImageToFormat(newImage, format)
	if err == nil {
		img.Image 	 = newImage
		img.Height 	 = height
		img.Width 	 = width
		img.Content  = newContent
		img.MimeType = GetContentMimeType(newContent)
	}
	return err
}

func encodeImageToFormat(image image.Image, format string) ([]byte, error) {
	var buffer bytes.Buffer
	var err error
	switch format {
		case "image/jpeg", "jpeg", "jpg":
			err = jpeg.Encode(bufio.NewWriter(&buffer), image, &jpeg.Options{Quality:100})

		case "image/png", "png":
			err = png.Encode(bufio.NewWriter(&buffer), image)

		default:
			panic("Unsupported image format. Types supported: jpeg, png")
	}
	return buffer.Bytes(), err
}