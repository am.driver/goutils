package goutils

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Param struct {
	key string
	valueStr string
	valueBool bool
	valueInt int
}

type ParamList struct {
	list map[string]Param
}

func newParamList() ParamList {
	return ParamList{list: make(map[string]Param)}
}

func (pl *ParamList) Add(param Param) {
	pl.list[param.key] = param
}

func (pl *ParamList) Get(param string) *Param {
	if p, exists := pl.list[param]; exists {
		return &p
	} else {
		return nil
	}
}

func (pl *ParamList) Exists(param string) bool {
	_, exists := pl.list[param]
	return exists
}

func (pl *ParamList) List() []Param {
	list := make([]Param, 0, len(pl.list))
	for _, param := range pl.list {
		list = append(list, param)
	}
	return list
}

func (pl *ParamList) Size() int {
	return len(pl.list)
}

func PrepareParams() ParamList {

	params := newParamList()

	for i, arg := range os.Args {
		if strings.HasPrefix(arg, "--") {
			key := strings.Replace(arg, "--", "", 1)
			parts := strings.Split(key, "=")
			if len(parts) == 1 {
				params.Add(prepareParam(parts[0], "true"))
			} else {
				params.Add(prepareParam(parts[0], parts[1]))
			}
		} else if strings.HasPrefix(arg, "-") {
			key := strings.Replace(arg, "-", "", 1)
			nextKey := os.Args[i+1]
			if strings.HasPrefix(nextKey, "-") {
				params.Add(prepareParam(key, "true"))
			} else {
				params.Add(prepareParam(key, nextKey))
			}
		}
	}

	return params
}

func prepareParam(keyName, keyValue string) Param {
	appKey := Param{key: keyName, valueStr: keyValue}
	boolVal, err := strconv.ParseBool(keyValue)
	if err == nil {
		appKey.valueBool = boolVal
	}

	intVal, err := strconv.Atoi(keyValue)
	if err == nil {
		appKey.valueInt = intVal
	}
	return appKey
}

func (key *Param) Name() string {
	return key.key
}

func (key *Param) Bool() bool {
	return key.valueBool
}

func (key *Param) Int() int {
	return key.valueInt
}

func (key *Param) String() string {
	return key.valueStr
}

func (key *Param) Print() {
	fmt.Println(key.key, key.String(), key.Int(), key.Bool())
}