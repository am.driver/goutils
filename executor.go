package goutils

import (
	"strings"
	"sync"
	"sync/atomic"
	"time"
)

type ChannelParallelExecutor struct {
	channel chan interface{}			// channel need to process
	threadsQuantity int					// quantity of parallel threads
	processor func(interface{}, int)	// function what to do with channel values
	waitTimeout int						// N seconds, waiting for channel new value before close execution.
										// Default is 0: executor will finish only when channel is closed
	isDone chan bool
	isWaited bool
	isExecutingNow bool
}

type ChannelExecutor struct {
	channel chan interface{}			// channel need to process
	processor func(interface{})			// function what to do with channel values
	waitTimeout int						// N seconds, waiting for channel new value before close execution.
	// Default is 0: executor will finish only when channel is closed
	processed int						// N processed values from channel
}

func NewChannelParallelExecutor(c chan interface{}, tq int, p func(interface{}, int)) *ChannelParallelExecutor {
	if cap(c) == 0 {
		panic("Channel must have buffer")
	}
	return &ChannelParallelExecutor{
		channel:c,
		threadsQuantity:tq,
		processor:p,
		waitTimeout:0,
		isDone:make(chan bool),
	}
}

func (ex *ChannelParallelExecutor) checkAccess() {
	if ex.isExecutingNow {
		panic("Can't setup executor while processing run")
	}
}

func (ex *ChannelParallelExecutor) SetWaitTimeout(wt int) {
	ex.checkAccess()
	ex.waitTimeout = wt
}

func (ex *ChannelParallelExecutor) Wait() {
	if !ex.isExecutingNow {
		panic("Wait() error: processing not run, nothing to waiting for")
	}
	ex.isWaited = true
	<- ex.isDone
}

func (ex *ChannelParallelExecutor) Finish() {
	defer func() {
		if r := recover(); r != nil && !strings.Contains(r.(error).Error(), "close of closed channel") {
			panic(r)
		}
	}()
	close(ex.channel)
}

func (ex *ChannelParallelExecutor) Execute() {

	ex.isExecutingNow = true
	var workingThreadsQty int32 = 0 // currently running working goroutines

	go func() {

		var wg sync.WaitGroup
		wg.Add(ex.threadsQuantity)

		defer func() {
			wg.Wait()
			ex.isExecutingNow = false
			if ex.isWaited {
				ex.isDone <- true
			}
			if len(ex.channel) > 0 {
				panic("Executor is done, but channel not empty")
			}
		}()

		for i := 1; i <= ex.threadsQuantity; i++ {
			go func(threadId int) {
				isWorkingThread := true
				atomic.AddInt32(&workingThreadsQty, 1)  // increment

				waitCounter    := 0
				waitLength     := 100
				maxWaitCounter := ex.waitTimeout * 1000 / waitLength

				for {
					select {
					case val, ok := <-ex.channel:
						if ok {
							waitCounter = 0
							// if this goroutine is sleeping - wake it up
							if !isWorkingThread && ex.waitTimeout > 0 {
								isWorkingThread = true
								atomic.AddInt32(&workingThreadsQty, 1)
							}
							ex.processor(val, threadId)
						} else {
							wg.Done()
							return
						}
					default:
						waitCounter++
						time.Sleep(time.Duration(waitLength) * time.Millisecond)
					}

					if waitCounter >= maxWaitCounter && ex.waitTimeout > 0 {
						if isWorkingThread {
							// set goroutine as sleeping, if it was the last working goroutine - finish execution
							isWorkingThread = false
							newWorkingThreadsQty := atomic.AddInt32(&workingThreadsQty, -1)
							if newWorkingThreadsQty == 0 {
								ex.Finish()
							}
						}
					}
				}
			}(i)
		}
	}()
}

// wt - waiting channel value timeout (sec)
func NewChannelExecutor(p func(interface{}), wt int) *ChannelExecutor {
	return &ChannelExecutor{
		channel:make(chan interface{}, 100000),
		processor:p,
		waitTimeout:wt,
		processed:0,
	}
}

func (ce *ChannelExecutor) Execute() {

	var wg sync.WaitGroup
	wg.Add(1)


	waitCounter := 0; waitLength := 100
	maxWaitCounter := ce.waitTimeout * 1000 / waitLength

	go func() {
		exec := true
		for exec {
			select {
			case x, ok := <-ce.channel:
				if ok {
					waitCounter = 0
					ce.processor(x)
					ce.processed++
				} else {
					exec = false
				}
			default:
				waitCounter++
				time.Sleep(time.Duration(waitLength) * time.Millisecond)
			}

			if ce.waitTimeout > 0 && waitCounter >= maxWaitCounter {
				ce.Finish()
			}
		}
		wg.Done()
	}()

	wg.Wait()
}

func (ce *ChannelExecutor) Channel() chan interface{} {
	return ce.channel
}

func (ce *ChannelExecutor) Processed() int {
	return ce.processed
}

func (ce *ChannelExecutor) Finish() {
	defer func() {
		if r := recover(); r != nil && !strings.Contains(r.(error).Error(), "close of closed channel") {
			panic(r)
		}
	}()
	close(ce.channel)
}