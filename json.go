package goutils

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/yalp/jsonpath"
	"reflect"
	"strconv"
)

type JsonObject struct {
	data interface{}
}

func ParseJson(jsonContent []byte) (*JsonObject, error) {
	var data interface{}
	err := json.Unmarshal(jsonContent, &data)
	if err != nil {
		return nil, err
	}
	return &JsonObject{data:data}, nil
}

func (j *JsonObject) readJson(path string) (interface{}, error) {
	result, err := jsonpath.Read(j.data, path)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (j *JsonObject) ReadJson(path string) (string, error) {
	iResult, err := j.readJson(path)
	if err != nil {
		return "", err
	}
	switch iResult.(type) {
		case string:
			return iResult.(string), nil
		case float64:
			return fmt.Sprintf("%f", iResult.(float64)), nil
		case int64:
			return strconv.FormatInt(iResult.(int64), 10), nil
		case int:
			return strconv.Itoa(iResult.(int)), nil
		default:
			return "", errors.New("result is not string: " + reflect.TypeOf(iResult).String())
	}
}

func (j *JsonObject) ReadJsonInt(path string) (int64, error) {
	iResult, err := j.readJson(path)
	if err != nil {
		return 0, err
	}
	switch iResult.(type) {
	case string:
		intResult, err := strconv.ParseInt(iResult.(string), 10, 64)
		return intResult, err
	case float64:
		return int64(iResult.(float64)), nil
	case int64:
		return iResult.(int64), nil
	case int:
		return iResult.(int64), nil
	default:
		return 0, errors.New("result is not int64: " + reflect.TypeOf(iResult).String())
	}
}