package goutils

import (
	"bufio"
	"io"
	"io/ioutil"
	"os"
	"strings"
)

func CheckFileExist(path string) bool {
	_, err := os.Stat(path)
	return err == nil
}

func WriteToFile(fileName string, content []byte) error {
	file, err := os.Create(fileName)

	if err == nil {
		defer func() {
			_ = file.Close()
		}()
		_, wrErr := file.Write(content)
		return wrErr
	}
	return err
}

func AppendToFile(path, line string) error {

	file, err := os.OpenFile(path, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err == nil {
		defer func() {
			_ = file.Close()
		}()
		_, err = file.WriteString(line + "\n")
	}
	return err
}

func ClearFileName(path string) string {
	return path[strings.LastIndex(path, "/")+1:]
}

func ReadFile(path string) ([]byte, error) {
	return ioutil.ReadFile(path)
}

func ReadPath(path string) ([]string, error) {
	if strings.Contains(path, "*") {
		parts := strings.SplitN(path, "*", 2)
		fileSuffix := strings.Replace(parts[1], "*", "", -1)
		filePrefix := parts[0][strings.LastIndex(parts[0], "/")+1:]
		pathToDir  := parts[0][:strings.LastIndex(parts[0], "/")+1]

		var filesResult []string
		filesAll, err := ReadDir(pathToDir)
		if err == nil {
			for _, file := range filesAll {
				fileName := strings.Replace(file, pathToDir, "", 1)
				if (strings.HasPrefix(fileName, filePrefix) || len(filePrefix) == 0) && (strings.HasSuffix(fileName, fileSuffix) || len(fileSuffix) == 0) {
					filesResult = append(filesResult, file)
				}
			}
		}
		return filesResult, err
	} else {
		return ReadDir(path)
	}
}

func ReadDir(path string) ([]string, error) {
	files, err := ioutil.ReadDir(path)
	if err == nil {
		var result []string
		for _, file := range files {
			if !file.IsDir() {
				result = append(result, path + file.Name())
			}
		}
		return result, nil
	}
	return nil, err
}

func ReadDirWithContent(path string) (map[string][]byte, error) {
	files, err := ReadDir(path)
	if err == nil {
		result := make(map[string][]byte)
		for _, file := range files {
			content, errf := ReadFile(file)
			if errf == nil {
				result[file] = content
			}
		}
		return result, nil
	}
	return nil, err
}

func ReadFileByLines(path string) []string {

	var result []string

	file, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	defer func() {
		_ = file.Close()
	}()

	reader := bufio.NewReader(file)

	for {
		line, _, err := reader.ReadLine()

		if err == io.EOF {
			break
		}

		if len(line) > 1 {
			result = append(result, string(line))
		}
	}

	return result
}

func ReadFileByLinesToChannel(path string, ch chan interface{}, closeOnFinish bool) {
	go func() {
		file, err := os.Open(path)
		if err != nil {
			panic(err)
		}
		defer file.Close()

		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			ch <- scanner.Text()
		}
		if closeOnFinish {
			close(ch)
		}
	}()
}