package goutils

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
)

type RedisDB struct {
	ctx context.Context
	host string
	port int
	client *redis.Client
}

func RedisOpen(host string, port int) *RedisDB {

	client  := redis.NewClient(&redis.Options {Addr: fmt.Sprintf("%s:%d", host, port)})
	redisDb := &RedisDB{ctx:client.Context(), host:host, port:port, client:client}

	stat := redisDb.client.Ping(redisDb.ctx)

	if stat != nil {
		_, err := stat.Result()
		if err != nil {
			panic(err)
		}
	} else {
		panic("Error redis connection")
	}

	return redisDb
}

func (rdb *RedisDB) Close() {
	_ = rdb.client.Close()
}

func (rdb *RedisDB) Host() string {
	return rdb.host
}

func (rdb *RedisDB) Port() int {
	return rdb.port
}

func (rdb *RedisDB) Llen(queue string) int64 {

	result, err := rdb.client.LLen(rdb.ctx, queue).Result()

	if err != nil {
		fmt.Println("Error LLEN: ", err)
	}
	return result
}

func (rdb *RedisDB) Publish(channel, message string) error {
	result := rdb.client.Publish(rdb.ctx, channel, message)
	return result.Err()
}