module gitlab.com/am.driver/goutils

go 1.16

require (
	github.com/go-redis/redis/v8 v8.8.2 // indirect
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646 // indirect
	github.com/yalp/jsonpath v0.0.0-20180802001716-5cc68e5049a0 // indirect
)
