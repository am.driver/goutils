package goutils

import (
	"archive/zip"
	"bytes"
	"io/ioutil"
	"time"
)

// Compress method compresses files into a single zip archive file
// Param: files is a map of files where key is a file name and value is a file content
// Return: zip archive bytes
func Compress(files map[string][]byte) ([]byte, error) {

	buffer := new(bytes.Buffer)

	zipWriter := zip.NewWriter(buffer)
	defer zipWriter.Close()

	for name, content := range files {
		header := &zip.FileHeader{
			Name: name,
			Method: zip.Store,
			ModifiedTime: uint16(time.Now().UnixNano()),
			ModifiedDate: uint16(time.Now().UnixNano()),
		}

		fw, err := zipWriter.CreateHeader(header)
		if err != nil {return nil, err}

		fw.Write(content)
	}

	zipWriter.Close()
	return buffer.Bytes(), nil
}


func Decompress(zipFile []byte) (map[string][]byte, error) {

	zipReader, err := zip.NewReader(bytes.NewReader(zipFile), int64(len(zipFile)))
	if err != nil {
		return nil, err
	}

	result := make(map[string][]byte)
	for _, file := range zipReader.File {
		content, err := readZipFile(file)

		if err != nil {
			return nil, err
		}

		result[file.Name] = content
	}
	return result, nil
}

func readZipFile(zipFile *zip.File) ([]byte, error) {
	file, err := zipFile.Open()
	defer file.Close()

	if err != nil {
		return nil, err
	}
	return ioutil.ReadAll(file)
}